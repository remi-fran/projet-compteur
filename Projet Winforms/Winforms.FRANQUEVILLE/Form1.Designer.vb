﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form remplace la méthode Dispose pour nettoyer la liste des composants.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requise par le Concepteur Windows Form
    Private components As System.ComponentModel.IContainer

    'REMARQUE : la procédure suivante est requise par le Concepteur Windows Form
    'Elle peut être modifiée à l'aide du Concepteur Windows Form.  
    'Ne la modifiez pas à l'aide de l'éditeur de code.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.decrement_btn = New System.Windows.Forms.Button()
        Me.raz_btn = New System.Windows.Forms.Button()
        Me.increment_btn = New System.Windows.Forms.Button()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.SuspendLayout()
        '
        'decrement_btn
        '
        Me.decrement_btn.Location = New System.Drawing.Point(243, 186)
        Me.decrement_btn.Name = "decrement_btn"
        Me.decrement_btn.Size = New System.Drawing.Size(82, 38)
        Me.decrement_btn.TabIndex = 0
        Me.decrement_btn.Text = "-"
        Me.decrement_btn.UseVisualStyleBackColor = True
        '
        'raz_btn
        '
        Me.raz_btn.Location = New System.Drawing.Point(378, 259)
        Me.raz_btn.Name = "raz_btn"
        Me.raz_btn.Size = New System.Drawing.Size(107, 40)
        Me.raz_btn.TabIndex = 1
        Me.raz_btn.Text = "RAZ"
        Me.raz_btn.UseVisualStyleBackColor = True
        '
        'increment_btn
        '
        Me.increment_btn.Location = New System.Drawing.Point(541, 186)
        Me.increment_btn.Name = "increment_btn"
        Me.increment_btn.Size = New System.Drawing.Size(80, 38)
        Me.increment_btn.TabIndex = 2
        Me.increment_btn.Text = "+"
        Me.increment_btn.UseVisualStyleBackColor = True
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 20.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.Location = New System.Drawing.Point(395, 112)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(75, 31)
        Me.Label1.TabIndex = 3
        Me.Label1.Text = "Total"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Font = New System.Drawing.Font("Microsoft Sans Serif", 35.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label2.Location = New System.Drawing.Point(409, 170)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(49, 54)
        Me.Label2.TabIndex = 4
        Me.Label2.Text = "0"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.Label2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.increment_btn)
        Me.Controls.Add(Me.raz_btn)
        Me.Controls.Add(Me.decrement_btn)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents decrement_btn As Button
    Friend WithEvents raz_btn As Button
    Friend WithEvents increment_btn As Button
    Friend WithEvents Label1 As Label
    Friend WithEvents Label2 As Label
End Class
