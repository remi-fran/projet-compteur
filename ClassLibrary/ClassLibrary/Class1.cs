﻿using System;

namespace ClassLibrary
{
    public class Compteur
    {
        public static int total = 0;

        public static void increment()
        {
            Compteur.total += 1;
        }
        public static void decrement()
        {
            Compteur.total -= 1;
        }
        public static void retourAZero()
        {
            Compteur.total = 0;
        }
    }
}
